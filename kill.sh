#!/bin/bash
source global.sh
source config.sh
set -u
WORKDIR=$1
EXPERIMENT=$2

cat $PIDFILE | xargs kill || true &> /dev/null

sleep 3
rm -f $PIDFILE
DEST=${MOUNTDIR}/${EXPERIMENT}
mkdir -p $DEST
#cp -rvf $WORKDIR/* ${MOUNTDIR}/$EXPERIMENT
#mv -v $WORKDIR $DEST
rsync -ruv * $DEST
rm -rv $WORKDIR
