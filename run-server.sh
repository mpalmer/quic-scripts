#!/bin/bash
set -Eeuo pipefail
source global.sh
source config.sh
source $1
PREFIX=$2
mkdir -p $(dirname $PREFIX)

$SRV_BIN $SRV_PARAMS &> $PREFIX.srv.out & echo "$!" >> $PIDFILE
wait
