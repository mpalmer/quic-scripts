#!/bin/bash

# use (probably) unique TMPDIR by default
TMPDIR=$(mktemp -d "/tmp/quic-$(date "+%Y-%m-%d_%H-%M-%S")-XXXX")
PIDFILE=$TMPDIR/pids

# load global configs
source configs/global.sh

EXPERIMENT=$(date "+%Y-%m-%d_%H-%M-%S")-$(basename $1 -config.sh )-server-$(basename $2 -config.sh)-client

source orchestrate-remote-server.sh
source orchestrate-local-client.sh

THIS=$(pwd)

function cleanup {
    echo "cleanup"
    PREFIX=$1
    cd $THIS
    srv_cleanup $PREFIX
    cd $THIS
    cli_cleanup $PREFIX
    cd $THIS
    sleep 10
}

function terminate {
    echo "terminate"
    cd $THIS
    srv_terminate
    cd $THIS
    cli_terminate
    cd $THIS
}

trap 'cleanup $PREFIX; terminate' INT

mkdir -p $TMPDIR

for PROTO in quic fquic rquic tcp
do
    cd $THIS
    echo "measuring $PROTO"
    if [[ $PROTO = fquic ]]
    then
        PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms_fd${FEC_D}_fp${FEC_P}/1"
    else
        PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms/1"
	  fi
    $SSH $SRV_HOST "cd $TMPDIR; ./run-server.sh config-${PROTO}.sh $PREFIX" &

    sleep 10

    cd $TMPDIR; ./run-client.sh config-${PROTO}.sh $PREFIX & A="$!"
    cd $THIS
    wait $A
    cleanup $PREFIX
done
cd $THIS
terminate
