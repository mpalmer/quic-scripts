WORKDIR="fquic"

FEC_D=6
FEC_P=2

SRV_PARAMS="${SRV_PARAMS} -FECpayloadSplit=${FEC_D} -FECredundancyPieces=${FEC_P} -enableFEC"
CLI_PARAMS="${CLI_PARAMS} -FECpayloadSplit=${FEC_D} -FECredundancyPieces=${FEC_P} -unreliableIFrames=false -unreliablePFrames=true -unreliableBFrames=true -unreliableXFrames=true"

PROTO="fquic"
