SRV_PORT=4243
SRV_PARAMS="${SRV_PARAMS} -useTCP -listenAddr=:${SRV_PORT}"
CLI_PARAMS="-serverAddr=${SRV_IP}:${SRV_PORT} -frameDelay=${CLI_PACING} -frameBufferLength=${CLI_BUFFLEN} -inFile=${INPUT_VIDEO} -pipe"
CLI_PARAMS="${CLI_PARAMS} -useTCP"
