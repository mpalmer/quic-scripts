MOUNTDIR="/vol/rechenknecht"
#TMPDIR="/tmp/quic"
#PIDFILE="${TMPDIR}/pids"
#INPUT_VIDEO="blue_sky_1080p24.mp4"
INPUT_VIDEO="v3_big_buck_inter_5000k_short.mp4"

DELAY=30
LOSS=0
BURST=0
REORDER=0
REORDERBURST=0
FEC_D=18
FEC_P=6
RATE="40Mbit"

SRV_BIN="bin/video-server-10mbit"
SRV_PARAMS="-mp4File=${INPUT_VIDEO}"

CLI_BIN="bin/video-client"
CLI_PACING=41
CLI_BUFFLEN=1

FFMPEG="bin/ffmpeg"
FFPROBE="bin/ffprobe"

SSH="ssh -F ssh/config"
SCP="scp -F ssh/config"
#SSH="ssh"
#SCP="scp"

HARPOON="/usr/local/harpoon/harpoon"

