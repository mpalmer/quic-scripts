SRV_IP="10.15.0.1"
SRV_PORT="4242"
SRV_HOST="inet-tmp-1-a"
SRV_BIN="bin/video-server-40mbit"

CLI_IP="10.15.0.3"
CLI_HOST="inet-tmp-1-c"
CLI_PARAMS="-serverAddr=${SRV_IP}:${SRV_PORT} -frameDelay=${CLI_PACING} -frameBufferLength=${CLI_BUFFLEN} -inFile=${INPUT_VIDEO} -pipe"

BRIDGE_HOST="inet-tmp-1-b"
BRIDGE_SRV_IF="eno2" #TODO, find out if this is true
BRIDGE_CLI_IF="enp3s0f0"
