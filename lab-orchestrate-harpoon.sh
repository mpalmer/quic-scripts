#!/bin/bash

source orchestrate.sh

EXPERIMENT=${EXPERIMENT}-harpoon


for RUN in $(seq 1 100)
do
    for HARPOON_RATE in 5 10 15 20
    do
        for PROTO in quic fquic rquic tcp
        do
            echo "$EXPERIMENT: measuring $PROTO with HARPOON rate of $HARPOON_RATE"
            if [[ $PROTO = fquic ]]
            then
                PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms_fd${FEC_D}_fp${FEC_P}_harpoon${HARPOON_RATE}/${RUN}"
            else
                PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms_harpoon${HARPOON_RATE}/${RUN}"
	          fi
            $SSH $BRIDGE_HOST "cd $TMPDIR; ./run-bridge.sh config-bridge.sh $PREFIX" &
            $SSH $SRV_HOST "cd $TMPDIR; ./run-server.sh config-${PROTO}.sh $PREFIX" &
            $SSH $SRV_HOST "cd $TMPDIR; ./run-harpoon.sh tcp_server_${HARPOON_RATE}.xml $PREFIX.srv" &

            sleep 10

            $SSH $CLI_HOST "cd $TMPDIR; ./run-client.sh config-${PROTO}.sh $PREFIX" & A="$!"
            $SSH $CLI_HOST "cd $TMPDIR; ./run-harpoon.sh tcp_client_${HARPOON_RATE}.xml $PREFIX.cli" &
            wait $A
            cleanup $PREFIX
        done
    done
done
terminate
