#!/bin/bash

set -Eeuo pipefail

function sleepuntilfullhour {
    WAIT=$((3600 - $(date "+%M") * 60 - $(date "+%S")))
    echo "sleeping for $WAIT seconds"
    sleep $WAIT
}

while sleepuntilfullhour
do
    source orchestrate.sh
    EXPERIMENT=${EXPERIMENT}-hourly
    for PROTO in tcp fquic rquic quic
    do
        echo "measuring $PROTO with loss of $LOSS"
        if [[ $PROTO = fquic ]]
        then
            PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms_fd${FEC_D}_fp${FEC_P}/${RUN}"
        else
            PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms/${RUN}"
	      fi
        #$SSH $BRIDGE_HOST "cd $TMPDIR; ./run-bridge.sh config-bridge.sh $PREFIX" &
        $SSH $SRV_HOST "cd $TMPDIR; ./run-server.sh config-${PROTO}.sh $PREFIX" &

        sleep 10

        $SSH $CLI_HOST "cd $TMPDIR; ./run-client.sh config-${PROTO}.sh $PREFIX" & A="$!"
        wait $A
        cleanup $PREFIX
    done
    terminate
done
