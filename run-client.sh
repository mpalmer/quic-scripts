#!/bin/bash
set -Eeuo pipefail
source global.sh
source config.sh
source $1
PREFIX=$2
mkdir -p $(dirname $PREFIX)
FFMPEG_PARAMS="-i ${INPUT_VIDEO} -i pipe: -lavfi \"ssim=${PREFIX}.new.ssim;[0:v][1:v]psnr\" -f null - "

#./$CLI_BIN $CLI_PARAMS 2> $PREFIX.cli.out | tee $PREFIX.mp4 | $FFMPEG $FFMPEG_PARAMS 2> $PREFIX.ffmpeg 1> /dev/null & echo "$!" >> $PIDFILE
./$CLI_BIN $CLI_PARAMS 2> $PREFIX.cli.out | tee $PREFIX.mp4 | $FFMPEG -i ${INPUT_VIDEO} -i pipe: -lavfi  "ssim=$PREFIX.new.ssim;[0:v][1:v]psnr" -f null - 2> $PREFIX.ffmpeg 1> /dev/null & echo "$!" >> $PIDFILE
wait
