#!/bin/bash

source orchestrate.sh

EXPERIMENT=${EXPERIMENT}-fairness

function bridgeconfig {
    video_size=$(stat --printf="%s" videos/$INPUT_VIDEO)
    video_length=$($FFPROBE -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 videos/$INPUT_VIDEO)
    video_rate=$(echo "(${video_size} / ${video_length}) * 8" | bc)
    rate_mult=$1
    RATE=$(echo "(${video_rate} * ${rate_mult})" | bc)
    RATE="${RATE}bit"
    # HACK
    echo "RATE=${RATE}" | $SSH $BRIDGE_HOST "cat - >> $TMPDIR/config-bridge.sh"
}

for RUN in $(seq 1 100)
do
    for RATE_MULT in 5 2.5 2 1.75
    do
        bridgeconfig $RATE_MULT
        for PROTO in quic fquic rquic tcp
        do
            echo "$EXPERIMENT: measuring $PROTO FAIRNESS with rate of $RATE"
            if [[ $PROTO = fquic ]]
            then
                PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms_fd${FEC_D}_fp${FEC_P}/${RUN}"
            else
                PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms/${RUN}"
	          fi
            $SSH $BRIDGE_HOST "cd $TMPDIR; ./run-bridge.sh config-bridge.sh $PREFIX" &
            $SSH $SRV_HOST "cd $TMPDIR; ./run-server.sh config-${PROTO}.sh $PREFIX" &
            $SSH $SRV_HOST "cd $TMPDIR; ./run-server.sh config-tcp-crosstraffic.sh $PREFIX.cross" &

            sleep 10

            $SSH $CLI_HOST "cd $TMPDIR; ./run-client.sh config-${PROTO}.sh $PREFIX" & A="$!"
            $SSH $CLI_HOST "cd $TMPDIR; ./run-client.sh config-tcp-crosstraffic.sh $PREFIX.cross" & B="$!"
            wait $A $B
            cleanup $PREFIX
        done
    done
done
terminate
