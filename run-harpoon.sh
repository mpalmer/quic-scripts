#!/bin/bash
set -Eeuo pipefail
source global.sh
source config.sh
HARPOON_CFG=$1
PREFIX=$2
mkdir -p $(dirname $PREFIX)

# HACK to obtain a killable PID we need to start the binary directly, not just the launcher script
export LD_LIBRARY_PATH=/usr/local/harpoon/plugins
${HARPOON} -v10 -w600 -c -f ${HARPOON_CFG} &> ${PREFIX}.harpoon.out & echo "$!" >> $PIDFILE
