#!/bin/bash
set -Eeuo pipefail


source $1

cat $1 | $SSH $SRV_HOST "mkdir -p $TMPDIR/bin; cd $TMPDIR; echo -e \"TMPDIR=$TMPDIR\nPIDFILE=$PIDFILE\" >> config.sh; cat - >> config.sh"

$SCP configs/*.sh run-server.sh kill.sh videos/${INPUT_VIDEO} *.pem $SRV_HOST:$TMPDIR
$SCP $SRV_BIN $SRV_HOST:$TMPDIR/bin

function publish_var_remotely {
    echo "$1=${$1}" | $SSH $SRV_HOST "cat - >> $TMPDIR/config.sh"
}

function srv_cleanup {
    pwd
    PREFIX=$1
    WORKDIR=$(dirname $PREFIX)
    $SSH $SRV_HOST "cd $TMPDIR; ./kill.sh $WORKDIR $EXPERIMENT"
}

function srv_terminate {
    pwd
    $SSH $SRV_HOST "rm -rvf $TMPDIR"
}


