#!/bin/bash

source orchestrate.sh

EXPERIMENT=${EXPERIMENT}-lossiness

function bridgeconfig {
    # HACK
    echo "LOSS=$LOSS" | $SSH $BRIDGE_HOST "cat - >> $TMPDIR/config-bridge.sh"
}

for RUN in $(seq 1 100)
do
    #for LOSS in 0 0.64 1.28 0.32 2.56 0.16 5.12 0.08 10.24 0.04 0.02 0.01
    for LOSS in 0 0.08 0.16 0.32 0.64 1.28 2.56 5.12
    do
        bridgeconfig $LOSS
        for PROTO in quic fquic rquic tcp
        do
            echo "$EXPERIMENT: measuring $PROTO with lossrate of $LOSS"
            if [[ $PROTO = fquic ]]
            then
                PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms_fd${FEC_D}_fp${FEC_P}/${RUN}"
            else
                PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms/${RUN}"
            fi
            $SSH $BRIDGE_HOST "cd $TMPDIR; ./run-bridge.sh config-bridge.sh $PREFIX" &
            $SSH $SRV_HOST "cd $TMPDIR; ./run-server.sh config-${PROTO}.sh $PREFIX" &

            sleep 10

            $SSH $CLI_HOST "cd $TMPDIR; ./run-client.sh config-${PROTO}.sh $PREFIX" & A="$!"
            wait $A
            cleanup $PREFIX
        done
    done
done
terminate
