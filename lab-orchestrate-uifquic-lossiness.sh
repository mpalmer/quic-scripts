#!/bin/bash
source orchestrate.sh

function bridgeconfig {
    # HACK
    echo "LOSS=$LOSS" | $SSH $BRIDGE_HOST "cat - >> $TMPDIR/config-bridge.sh"
}

EXPERIMENT=${EXPERIMENT}-uifquic-lossiness

for RUN in $(seq 1 100)
do
    for LOSS in 0.64 1.28 0 1.0 0.32 2.56
    do
        bridgeconfig $LOSS
        for PROTO in fquic uifquic
        do
            echo "measuring $PROTO with loss of $LOSS"
            if [[ $PROTO = fquic ]]
            then
                PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms_fd${FEC_D}_fp${FEC_P}/${RUN}"
            else
                PREFIX="${PROTO}_${RATE}_${DELAY}ms_l${LOSS}p_b${BURST}p_p${CLI_PACING}ms/${RUN}"
	          fi
            $SSH $BRIDGE_HOST "cd $TMPDIR; ./run-bridge.sh config-bridge.sh $PREFIX" &
            $SSH $SRV_HOST "cd $TMPDIR; ./run-server.sh config-${PROTO}.sh $PREFIX" &

            sleep 10

            $SSH $CLI_HOST "cd $TMPDIR; ./run-client.sh config-${PROTO}.sh $PREFIX" & A="$!"
            wait $A
            cleanup $PREFIX
        done
    done
done
terminate
