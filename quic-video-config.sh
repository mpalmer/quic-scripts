SRV_IP="10.15.0.1"
SRV_PORT="4242"
SRV_HOST="quic-video-1"

CLI_IP="10.15.0.3"
CLI_HOST="quic-video-3"
CLI_PARAMS="-serverAddr=${SRV_IP}:${SRV_PORT} -frameDelay=${CLI_PACING} -frameBufferLength=${CLI_BUFFLEN} -inFile=${INPUT_VIDEO} -pipe"

BRIDGE_HOST="quic-video-2"
BRIDGE_SRV_IF="eno3" #TODO, find out if this is true
BRIDGE_CLI_IF="eno4"
