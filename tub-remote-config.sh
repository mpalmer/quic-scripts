INPUT_VIDEO="v3_big_buck_inter_700k_480p_inter_short.mp4"

SRV_IP="130.149.220.181"
SRV_HOST="${SRV_IP}"
SRV_PORT="4242"
SRV_PARAMS="-mp4File=${INPUT_VIDEO}"

CLI_PARAMS="-serverAddr=${SRV_IP}:${SRV_PORT} -frameDelay=${CLI_PACING} -frameBufferLength=${CLI_BUFFLEN} -inFile=${INPUT_VIDEO} -pipe"
