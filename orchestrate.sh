#!/bin/bash
set -Eeuo pipefail

TMPDIR=$(mktemp -d "/tmp/quic-$(date "+%Y-%m-%d_%H-%M-%S")-XXXX")
PIDFILE=$TMPDIR/pids

# load global configs
source configs/global.sh

# specify local config
CONFIG=$1
source $CONFIG


$SSH $SRV_HOST "mkdir -p $TMPDIR"
$SSH $CLI_HOST "mkdir -p $TMPDIR"
# test if $BRIDGE_HOST is set
[[ -v BRIDGE_HOST ]] && $SSH $BRIDGE_HOST "mkdir -p $TMPDIR"

$SCP -r configs/* run-harpoon.sh run-server.sh kill.sh videos/${INPUT_VIDEO} bin *.pem $SRV_HOST:$TMPDIR
# it's probably smart to archive every sh script that's lying around here
# FIXME yes, this is redundant but I don't have time right now to do this efficiently
$SCP *.sh $SRV_HOST:$TMPDIR
$SCP $CONFIG $SRV_HOST:$TMPDIR/config.sh
$SCP -r configs/* run-harpoon.sh run-client.sh kill.sh videos/${INPUT_VIDEO} bin $CLI_HOST:$TMPDIR
$SCP $CONFIG $CLI_HOST:$TMPDIR/config.sh
[[ -v BRIDGE_HOST ]] && $SCP -r configs/* run-bridge.sh kill.sh monitor-tc.sh $BRIDGE_HOST:$TMPDIR
[[ -v BRIDGE_HOST ]] && $SCP $CONFIG $BRIDGE_HOST:$TMPDIR/config.sh

EXPERIMENT=$(date "+%Y-%m-%d_%H-%M-%S")-$(basename $CONFIG .sh)

function publish_var_remotely {
    echo "$1=${!1}" | $SSH $SRV_HOST "cat - >> $TMPDIR/config.sh"
    echo "$1=${!1}" | $SSH $CLI_HOST "cat - >> $TMPDIR/config.sh"
    [[ -v BRIDGE_HOST ]] && echo "$1=${!1}" | $SSH $BRIDGE_HOST "cat - >> $TMPDIR/config.sh"
}

publish_var_remotely TMPDIR
publish_var_remotely PIDFILE


function cleanup {
    PREFIX=$1
    WORKDIR=$(dirname $PREFIX)
    $SSH $CLI_HOST "cd $TMPDIR; ./kill.sh $WORKDIR $EXPERIMENT"
    $SSH $SRV_HOST "cd $TMPDIR; ./kill.sh $WORKDIR $EXPERIMENT"
    [[ -v BRIDGE_HOST ]] && $SSH $BRIDGE_HOST "cd $TMPDIR; ./kill.sh $WORKDIR $EXPERIMENT"
    sleep 10
}

function terminate {
    $SSH $CLI_HOST "rm -rvf $TMPDIR"
    $SSH $SRV_HOST "rm -rvf $TMPDIR"
    [[ -v BRIDGE_HOST ]] && $SSH $BRIDGE_HOST "rm -rvf $TMPDIR"
}

trap 'cleanup $PREFIX; terminate' INT


