#!/bin/bash
set -Eeuo pipefail


# specify local config
source $1
source $2

mkdir -p $TMPDIR/bin

echo "TMPDIR=$TMPDIR" >> $TMPDIR/config.sh
echo "PIDFILE=$PIDFILE" >> $TMPDIR/config.sh
cat $1 $2 >> $TMPDIR/config.sh
cp configs/*.sh run-harpoon.sh run-client.sh kill.sh videos/${INPUT_VIDEO} $TMPDIR
cp ${FFMPEG} ${CLI_BIN} $TMPDIR/bin

function cli_cleanup {
    PREFIX=$1
    WORKDIR=$(dirname $PREFIX)
    cd $TMPDIR; ./kill.sh $WORKDIR $EXPERIMENT
}

function cli_terminate {
    rm -rvf $TMPDIR
}
